﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Bullet
{
    internal class Shot1Bullet : BulletBase
	{
        public int ReflectCount=5;

        private int m_currentReflectCount = 0;

        protected override void OnCollisionEnter(Collision collision)
        {
            base.OnCollisionEnter(collision);

            m_currentReflectCount++;
            if (m_currentReflectCount >= ReflectCount)
            {
                GameObject.Destroy(gameObject);
            }
        }
    }
}