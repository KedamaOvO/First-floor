﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HuangRongMove : MonoBehaviour {
    public bool isHited = false;
    private Animator m_animator;
    private CharacterController m_charControl;
    private int m_hitNum = 0;
    private float m_lastTime = 0f;
    public int bleed = 1000;
    private int m_temp = 0;

    void Start () {
        m_animator = GetComponent<Animator>();
        m_charControl = GetComponent<CharacterController>();
	}
	
	// Update is called once per frame
	void Update () {
        if (GetComponent<MonsterAI>().HealthPoint == 0)
        {
            this.enabled = false;
            
        }
        if (!m_animator.GetCurrentAnimatorStateInfo(0).IsName("born01")) {
            m_charControl.enabled = true;
        }
        //倒下时不可收到攻击
        if (m_animator.GetCurrentAnimatorStateInfo(0).IsName("hurt_02") || m_animator.GetCurrentAnimatorStateInfo(0).IsName("hurt02_2") || m_animator.GetCurrentAnimatorStateInfo(0).IsName("hurt02_3")) {
            m_charControl.enabled = false;
        }
        if (isHited) {
            bleed -= 20;
            m_animator.CrossFade("hurt_01", 0.2f);
            isHited = false;
            if (m_lastTime == 0f)
            {
                m_lastTime = Time.time;
            }
            else
            {
                Debug.Log(Time.time - m_lastTime);
                
                if ((Time.time - m_lastTime) < 1.2f)
                {
                    m_hitNum++;
                }
                else {
                    m_hitNum = 1;
                }
                if (m_hitNum == 8) {
                    m_animator.CrossFade("hurt_02", 0.2f);
                    m_hitNum = 0;
                }
                m_lastTime = Time.time;
            }
        }
        
	}
    void OnControllerColliderHit(ControllerColliderHit hit) {
        
    }
}
