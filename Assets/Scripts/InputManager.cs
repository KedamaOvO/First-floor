﻿using Player;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Manager
{
    internal static class InputManager
    {
        private static bool m_enable = true;
        public static bool Enable{
            get { return m_enable; }
            set { m_enable = value; }
        }

        private static bool m_xDown = false;
        private static bool m_yDown = false;
        private static Transform m_cameraTransform;
        private static Transform m_playerTransform;

        static InputManager()
        {
            m_cameraTransform = GameObject.FindWithTag("MainCamera").transform;
            m_playerTransform = GameObject.FindWithTag("Player").transform;
        }

        public static float GetAxis(string s)
        {
            return m_enable?Input.GetAxis(s):0.0f;
        }
        public static float GetAxisRaw(string s)
        {
            return m_enable?Input.GetAxisRaw(s):0.0f;
        }
        public static bool GetButtonDown(string key)
        {
            if (!m_enable) return false;

            if ((key == "Forward" || key == "Back"))
            {
                if (m_xDown == true) return false;

                Vector3 dir = new Vector3(Input.GetAxis("Horizontal"), 0, 0);
                dir = m_cameraTransform.TransformDirection(dir);
                float a = Vector3.Dot(m_playerTransform.forward, dir);

                if (a > 0 && key == "Forward")
                {
                    m_xDown = true;
                    return true;
                }
                else if (a < 0 && key == "Back")
                {
                    m_xDown = true;
                    return true;
                }
                else
                {
                    m_xDown = false;
                    return false;
                }
            }
            else if ((key == "Up" || key == "Down"))
            {
                if (m_yDown == true) return false;

                float y = Input.GetAxis("Vertical");
                if (y < 0 && key == "Down")
                {
                    m_yDown = true;
                    return true;
                }
                else if (y > 0 && key == "Up")
                {
                    m_yDown = true;
                    return true;
                }
                else
                {
                    m_yDown = false;
                    return false;
                }
            }
            else
            {
                return Input.GetButtonDown(key);
            }
        }
        public static bool GetButtonUp(string key)
        {
            if (!m_enable) return false;

            if (key == "Forward" || key == "Back")
            {
                if (m_xDown == false) return false;

                float x = Input.GetAxis("Horizontal");
                if (Mathf.Abs(x) <= float.Epsilon)
                {
                    m_xDown = false;
                    return true;
                }
                else return false;
            }
            else if (key == "Up" || key == "Down")
            {
                if (m_yDown == false) return false;

                float y = Input.GetAxis("Vertical");
                if (Mathf.Abs(y) <= float.Epsilon)
                {
                    m_yDown = false;
                    return true;
                }
                else return false;
            }
            else
            {
                return Input.GetButtonUp(key);
            }
        }
        public static bool GetButton(string k)
        {
            return m_enable ? Input.GetButton(k) : false;
        }
    }
}


