﻿using Entity;
using Manager;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Player
{
    public class PlayerMove : MonoBehaviour
    {
        private PlayerEntity m_player;

        private Transform m_mainCamera;
        public float Speed = 5.0f;
        public float JumpHeight = 2.5f;
        public float SecondJumpHeight = 1.5f;
        public float Gravity = 9.8F;
        public float InputRecoverTime = 0.15f;

        CharacterController m_controller;
        Animator m_animator;

        private Vector3 m_moveVelocity = Vector3.zero;
        private Vector3 m_jumpVelocity = Vector3.zero;
        private Vector3 m_gravityVelocity = Vector3.zero;
        private Vector3 m_Velocity = Vector3.zero;

        private void Awake()
        {
            m_player = GetComponent<PlayerEntity>();
            m_mainCamera = GameObject.FindWithTag("MainCamera").transform;
            m_controller = GetComponentInChildren<CharacterController>();
            m_animator = GetComponentInChildren<Animator>();

            m_player.OnStatusChanged += (l, c) =>
              {
                  if(!EntityBase.HasStatusFlag(l,EntityStatus.Grounded)&&
                    EntityBase.HasStatusFlag(c,EntityStatus.Grounded))
                  {
                      InputManager.Enable = false;
                      Invoke("EnableInput", InputRecoverTime);
                  }
              };
        }

        private void EnableInput()
        {
            InputManager.Enable = true;
        }

        void Update()
        {
            m_moveVelocity = new Vector3(InputManager.GetAxisRaw("Horizontal"), 0, 0);
            m_moveVelocity = m_mainCamera.TransformDirection(m_moveVelocity);
            m_moveVelocity *= Speed;


            if (m_controller.isGrounded)
            {
                m_jumpVelocity = Vector3.zero;
                m_gravityVelocity = Vector3.zero;

                m_animator.SetBool("jump02", false);
                m_animator.SetBool("jump01", false);

                m_player.ResetStatusFlag(EntityStatus.FirstJump);
                m_player.ResetStatusFlag(EntityStatus.SecondJump);
                m_player.SetStatusFlag(EntityStatus.Grounded);

                transform.LookAt(m_moveVelocity + transform.position);

                if (InputManager.GetButtonDown("Jump"))
                {
                    m_player.SetStatusFlag(EntityStatus.FirstJump);
                    m_animator.SetBool("jump01", true);
                    m_animator.SetBool("move", false);

                    m_jumpVelocity = Vector3.up * Mathf.Sqrt(2*JumpHeight*Gravity);
                }

            }
            else
            {
                m_player.ResetStatusFlag(EntityStatus.Grounded);

                if(m_player.HasStatusFlag(EntityStatus.FirstJump)&&InputManager.GetButtonDown("Jump"))
                {
                    m_player.ResetStatusFlag(EntityStatus.FirstJump);
                    m_player.SetStatusFlag(EntityStatus.SecondJump);

                    m_animator.SetBool("jump02", true);
                    m_animator.SetBool("jump01", false);

                    m_gravityVelocity = Vector3.zero;
                    m_jumpVelocity = Vector3.up * Mathf.Sqrt(2*SecondJumpHeight*Gravity);
                }
            }

            m_Velocity = m_moveVelocity + m_jumpVelocity;
            m_gravityVelocity += Vector3.down * Gravity * Time.deltaTime;

            m_controller.Move((m_Velocity+m_gravityVelocity) * Time.deltaTime);

            //前进状态判断
            float s = Mathf.Abs(m_moveVelocity.magnitude);

            if (s != 0)
            {
                float a = Vector3.Dot(transform.forward, m_moveVelocity);
                if (a > 0)
                {
                    m_player.ResetStatusFlag(EntityStatus.Backward);
                    m_player.SetStatusFlag(EntityStatus.Forward);
                }
                else
                {
                    m_player.ResetStatusFlag(EntityStatus.Forward);
                    m_player.SetStatusFlag(EntityStatus.Backward);
                }
                if(!m_player.HasStatusFlag(EntityStatus.FirstJump)&&
                    !m_player.HasStatusFlag(EntityStatus.SecondJump))
                    m_animator.SetBool("move", true);
            }
            else
            {
                m_player.ResetStatusFlag(EntityStatus.Forward | EntityStatus.Backward);
                m_animator.SetBool("move", false);
            }
        }
    }
}

