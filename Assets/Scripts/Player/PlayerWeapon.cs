﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Entity;

public class PlayerWeapon : MonoBehaviour {
    public GameObject player;
    private Animator m_animator;
    private int m_temp=4;
    
    
	// Use this for initialization
	void Start () {
        m_animator = player.GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
        
        if (m_temp < 3)
        {
            m_temp++;
        }
        else if (m_temp == 3) {
            m_animator.speed = 1f;
            m_temp++;
        }
	}

    //武器的碰撞事件
    void OnTriggerEnter(Collider co)
    {
        //Debug.Log(co.gameObject.name);
        if (co.gameObject.tag == "Monster") {
            //co.gameObject.transform.LookAt(new Vector3(transform.position.x, co.gameObject.transform.position.y, transform.position.z));
            co.gameObject.GetComponent<HuangRongMove>().isHited = true;
            m_animator.speed = 0;
            m_temp = 0;
            Debug.Log("hit");
            co.gameObject.GetComponent<EntityBase>().TakeDamage(10);
            
        }
    }
}
