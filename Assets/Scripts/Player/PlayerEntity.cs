﻿using Attributes;
using Entity;
using Skill;
using Skill.Test;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Player
{
    internal class PlayerEntity : EntityBase
    {
        [ReadOnly]
        public string StatusDebug;
        [ReadOnly]
        public string SkillDebug;

        public static PlayerEntity Instance;

        private void OnEnable()
        {
            if (Instance != null)
                throw new UnityException("PlayerEntity.Instance!=null");
            Instance = this;
        }

        private void OnDisable()
        {
            Instance = null;
        }

        //单例
        private PlayerSkillManager m_playerSkillManager;

        // Use this for initialization
        protected override void Start()
        {
            m_playerSkillManager = new PlayerSkillManager(this, m_animator);
            SkillManager = m_playerSkillManager;

            PlayerSkillManager.Instance.BindSkill<NormalAtack>("Touch");
            PlayerSkillManager.Instance.BindSkill<NormalAtack>("Thump");
            //PlayerSkillManager.Instance.BindHoldSkill<HoldTestSkill>("Thump");
            PlayerSkillManager.Instance.BindComboSkill<ComboTestSkill>(new string[] { "Thump", "Thump","Touch", "Thump" },EntityStatus.SecondJump);
            PlayerSkillManager.Instance.BindComboSkill<ComboTestSkill2>(new string[] { "Forward", "Touch", "Touch" },EntityStatus.Grounded);
            //PlayerSkillManager.Instance.BindHoldSkill<RemoteNormalSkill>("NormalShot");
            PlayerSkillManager.Instance.BindSkill<TransferSkillPart1>("Touch", EntityStatus.Movement|EntityStatus.Grounded,StatusMatchOp.One,true, "TransferShot");
            //PlayerSkillManager.Instance.BindSkill<TransferSkillPart2>("Transfer");
            //PlayerSkillManager.Instance.BindHoldSkill<ProjectileSkill>("ProjectileShot"); 

            base.OnHurt += (lasr, cur) =>
              {
                  if (base.HasStatusFlag(EntityStatus.TakeHit))
                  {
                      m_animator.SetTrigger("Hurt");
                  }
              };

            base.Start();
        }

        protected override void Update()
        {
            base.Update();
            StatusDebug = Status.ToString();

            var k = PlayerSkillManager.Instance.GetActivateSkill();
            if (k != null)
                SkillDebug = k.ToString();
            else
                SkillDebug = "";
        }
    }
}
