﻿using Entity;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Skill.Context;

namespace Skill
{
    /// <summary>
    /// 除Player外的Entity用
    /// </summary>
    internal class SkillManager
    {
        protected EntityBase m_entity;
        protected Animator m_animator;

        private LinkedList<SkillContext> m_skillList=new LinkedList<SkillContext>();

        public SkillManager(EntityBase entity, Animator animator)
        {
            m_entity = entity;
            m_animator = animator;
        }

        /// <summary>
        /// 给实体注册技能
        /// </summary>
        /// <typeparam name="T">继承SkillBase的技能类</typeparam>
        public SkillContext RegisterSkill<T>() where T : ISkill, new()
        {
            ISkill skill = new T();
            SkillContext skill_ctx = new SkillContext(m_entity, skill);

            if (m_skillList == null)
                m_skillList = new LinkedList<SkillContext>();
            m_skillList.AddLast(skill_ctx);
            return skill_ctx;
        }

        /// <summary>
        /// 给实体注册蓄力技能
        /// </summary>
        /// <typeparam name="T">继承SkillBase的技能类</typeparam>
        public HoldSkillContext RegisterHoldSkill<T>() where T : ISkill,ISkillHoldable, new()
        {
            ISkill skill = new T();

            HoldSkillContext skill_ctx = new HoldSkillContext(m_entity, skill);

            if (m_skillList == null)
                m_skillList = new LinkedList<SkillContext>();
            m_skillList.AddLast(skill_ctx);
            return skill_ctx;
        }

        /// <summary>
        /// 获取正在使用的技能
        /// </summary>
        /// <returns></returns>
        public ISkill GetActivateSkill()
        {
            if (m_skillList == null)
            {
                throw new UnityException("No Registered Skill");
            }

            AnimatorStateInfo current_status = m_animator.GetCurrentAnimatorStateInfo(0);
            AnimatorStateInfo next_status = m_animator.GetNextAnimatorStateInfo(0);

            foreach (var skill_ctx in m_skillList)
            {
                if (Animator.StringToHash(skill_ctx.Skill.AnimationName) == current_status.shortNameHash)
                {
                    return skill_ctx.Skill;
                }
                else if (Animator.StringToHash(skill_ctx.Skill.AnimationName) == next_status.shortNameHash)
                {
                    return skill_ctx.Skill;
                }
            }
            return null;
        }

        public virtual void Update(EntityStatus currentStatus)
        {
            foreach(var ctx in m_skillList)
            {
                ctx.Update();
            }
        }

        public SkillContext FindContext(ISkill skill)
        {
            foreach(var n in m_skillList)
            {
                if (n.Skill.GetType() == skill.GetType())
                    return n;
            }
            return null;
        }

        public IEnumerator<SkillContext> GetEnumerator()
        {
            return m_skillList.GetEnumerator();
        }
    }
}
