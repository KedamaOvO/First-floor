﻿using Attributes;
using Entity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Skill.Context
{
    /// <summary>
    /// 隐藏一些技能状态的中间类
    /// </summary>
    internal class SkillContext
    {
        public ISkill Skill { get; protected set; }
        public EntityBase Self { get; set; }

        [SerializeField, ReadOnly]
        protected float m_remainCoolingTime = 0.0f;
        public float RemainCoolingTime { get { return m_remainCoolingTime; } }

        private const float s_clear_timer = 0.6f;
        private float m_clear_timer = s_clear_timer;

        protected Animator m_animator;

        public SkillContext(EntityBase self, ISkill skill)
        {
            Self = self;
            Skill = skill;
            m_animator = self.GetComponentInChildren<Animator>();
        }

        public void Update()
        {
            if (m_clear_timer < 0.0f)
            {
                Reset();
            }
            else
            {
                m_clear_timer -= Time.fixedDeltaTime;
            }

            if(m_remainCoolingTime>0.0f)
            {
                m_remainCoolingTime -= Time.fixedDeltaTime;
            }
        }

        public virtual void Use()
        {
            if (m_remainCoolingTime > 0) return;

            ResetClearTime();
            Skill.OnUse(Self,m_animator);

            m_remainCoolingTime = Skill.CoolingTime;
        }

        /// <summary>
        /// 重置ResetTime,ResetTime小于等于0时OnReset被调用
        /// </summary>
        protected void ResetClearTime()
        {
            m_clear_timer = s_clear_timer;
        }

        protected virtual void Reset()
        {
            Skill.OnReset(m_animator);
        }
    };
}
