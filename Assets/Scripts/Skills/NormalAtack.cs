﻿using Entity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Skill
{
    internal class NormalAtack : SkillBase
    {
        private static readonly string[] s_animationNames = new string[] { "NormalAttack1", "NormalAttack2", "NormalAttack3" };

        private string m_animationName = s_animationNames[0];
        private int i = 0;

        public NormalAtack()
        {
            AnimationName = s_animationNames[0];
            CoolingTime = 0.0f;
        }

        public override void OnUse(EntityBase self, Animator animator)
        {
            if (i >= 3)
            {
                i = 0;
                return;
            }

            animator.SetTrigger(s_animationNames[i] + "Trigger");
            AnimationName = s_animationNames[i];
            Hit = i + 1;
            i++;
        }

        public override void OnReset(Animator animator)
        {
            foreach (string tri in s_animationNames)
                animator.ResetTrigger(tri + "Trigger");
            i = 0;
        }

        public override void OnHit(EntityBase self, EntityBase target)
        {
            ApplyFormula(self, target);
        }
    }
}
