﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CirclePoint : MonoBehaviour {
	private GameObject gb;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(gb!=null)
		Debug.DrawLine (new Vector3(transform.position.x-25f,gb.transform.position.y,transform.position.z+25f), gb.transform.position, Color.red);
	}

	void OnTriggerEnter(Collider co) {
		if (co.gameObject.tag == "Player") {
			co.gameObject.GetComponent<TestCircleMove> ().isCircleMoving = true;
			gb = co.gameObject;
			co.gameObject.GetComponent<TestCircleMove> ().circlePoint = new Vector3(transform.position.x-25f,co.gameObject.transform.position.y,transform.position.z+25f);

		}
    }
	void OnTriggerExit(Collider co) {
		if (co.gameObject.tag == "Player") {
			co.gameObject.GetComponent<TestCircleMove> ().isCircleMoving = false;
		}
	}
}
