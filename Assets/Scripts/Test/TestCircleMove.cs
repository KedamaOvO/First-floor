﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestCircleMove : MonoBehaviour {
	public bool isCircleMoving = false;
	public Vector3 circlePoint=new Vector3(0,0,0);
	private float speed = 10f;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (isCircleMoving == true) {
			float distance=Vector3.Distance (circlePoint, transform.position);
			float angle = speed * Time.deltaTime * 180 / (Mathf.PI * distance);
			if (Input.GetKey (KeyCode.D)) {
				transform.RotateAround (circlePoint, transform.up, -angle);

			}
			if (Input.GetKey (KeyCode.A)) {
				transform.RotateAround (circlePoint, transform.up, angle);
			}
		} else {
			if (Input.GetKey(KeyCode.D)) {
				transform.Translate(transform.forward * Time.deltaTime * speed,Space.World);
			}
			if (Input.GetKey (KeyCode.A)) {
				transform.Translate(transform.forward * Time.deltaTime * -speed,Space.World);
			}
		}
        
	}
}
