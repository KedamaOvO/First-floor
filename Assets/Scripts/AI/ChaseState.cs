﻿using Entity;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ChaseState : FSMState
{
    private List<Transform> path = new List<Transform>();
  //  private int index = 0;

    private EntityBase m_player;
    private EntityBase m_self;
    private Transform playerTransform;

    private Animator m_animator;
    private NavMeshAgent m_agent;
    private CharacterController m_controller;

    public ChaseState(FSMSystem fsm, GameObject obj) : base(fsm)
    {
        m_animator = obj.GetComponent<Animator>();
        m_agent = obj.GetComponent<NavMeshAgent>();
        m_controller = obj.GetComponent<CharacterController>();
        m_self = obj.GetComponent<EntityBase>();

        stateID = StateID.Chase;

        playerTransform = GameObject.Find("Player").transform;
        Transform pathTransform = GameObject.Find("Path").transform;
        m_player = playerTransform.GetComponent<EntityBase>();

        Transform[] children = pathTransform.GetComponentsInChildren<Transform>();
        foreach (Transform child in children)
        {
            if (child != pathTransform)
            {
                path.Add(child);
            }
        }
        playerTransform = GameObject.Find("Player").transform;
        m_agent.autoTraverseOffMeshLink = false;
        m_agent.updatePosition = false;
        m_agent.updateRotation = false;
    }

    private enum Status
    {
        Chase, Jump,Rotate,Downstairs
    }

    private const float speed = 10.0f;

    private bool _isGrounded=true;
    private Vector3 _moveDirection = Vector3.zero;
    private Status _status = Status.Chase;
    private Quaternion _target_rotate;
    NavMeshPath _path = new NavMeshPath();

    public override void Act(GameObject npc, Animator ani)
    {
        if (_status == Status.Chase)
        {
            m_agent.CalculatePath(playerTransform.position, _path);

            if (_path.status==NavMeshPathStatus.PathComplete)
            {
                RaycastHit rhit;
                if (!Physics.Raycast(playerTransform.position, Vector3.down, out rhit,100f)) return;

                var dir = rhit.point - npc.transform.position;
                dir.y = 0;
                dir.Normalize();

                float a = Vector3.Dot(dir, npc.transform.forward);

                if (a>=(-1+10e-6))//同向
                {
                    NavMeshHit hit;
                    m_agent.FindClosestEdge(out hit);

                    if (hit.distance < 0.1f)
                    {
                        npc.GetComponent<Enemy>().Jump();
                        _status = Status.Jump;
                    }
                    else
                    {
                        var t = npc.transform.forward * speed * Time.deltaTime;
                        m_controller.Move(t);
                    }
                }
                else if(Vector3.Dot(Vector3.down, (rhit.point - npc.transform.position).normalized)>0.85f&&m_self.IsGrounded)//在player正上方
                {
                    if(_path.corners.Length!=0)
                    {
                        _moveDirection = _path.corners[1] - _path.corners[0];
                        _moveDirection.y = 0;
                        _moveDirection.Normalize();
                        Debug.Log(_moveDirection);
                        _status = Status.Downstairs;
                    }
                }
                else//反向
                {
                    _status = Status.Rotate;
                    var rotate = Quaternion.FromToRotation(npc.transform.forward, -npc.transform.forward);
                    _target_rotate = npc.transform.rotation * rotate;
                }
            }
            else//player跳跃情况
            {
                var t = npc.transform.forward * speed * Time.deltaTime;
                m_controller.Move(t);
            }
        }
        else if (_status == Status.Rotate)
        {
            var r = Quaternion.RotateTowards(npc.transform.rotation, _target_rotate, 3.0f);
            npc.transform.rotation = r;

            float dot = Quaternion.Dot(r, _target_rotate);

            if (dot < (1 + 10e-7) && dot > (1 - 10e-7))
            {
                npc.transform.position += npc.transform.forward * speed * Time.deltaTime;
                npc.transform.rotation = _target_rotate;
                _status = Status.Chase;
            }
        }
        else if (_status == Status.Jump)
        {
            if (m_self.IsGrounded)
            {
                _status = Status.Chase;
            }
        }
        else if (_status == Status.Downstairs)
        {
            var t = _moveDirection * speed * Time.deltaTime;
            m_controller.Move(t);

            if(m_self.IsGrounded == true&&_isGrounded==false)//认为接触地面
            {
                _status = Status.Chase;
            }
            _isGrounded = m_self.IsGrounded;
        }
    }

    public override void Reason(GameObject npc, Animator ani)
    {
        if (_status !=Status.Chase) return;
        if (!m_player.IsGrounded) return;

        if (Vector3.Distance(playerTransform.position, npc.transform.position) > 25||_path.status!=NavMeshPathStatus.PathComplete)
        {
            fsm.PerformTransition(Transition.LostPlayer);
            ani.SetFloat("speed", 1.0f);
        }
    }
}