﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


public class PatrolState : FSMState
{
    
    private List<Transform> path = new List<Transform>();
    private int index = 0;
    private Transform playerTransform;

    private Animator m_animator;

    private CharacterController m_controller;
    private NavMeshAgent m_agent;

    public PatrolState(FSMSystem fsm, GameObject obj) : base(fsm)
    {
        m_animator = obj.GetComponent<Animator>();
        m_agent = obj.GetComponent<NavMeshAgent>();
        m_controller = obj.GetComponent<CharacterController>();

        stateID = StateID.Patrol;

        playerTransform = GameObject.Find("Player").transform;
        m_animator.SetFloat("speed", 1.0f);
    }

    private enum Status
    {
        Front, Rotate
    }

    private Status _status = Status.Front;
    private Quaternion _target_rotate;

    //巡逻
    public override void Act(GameObject npc, Animator ani)
    {
        if (_status == Status.Front)
        {
            NavMeshHit hit;
            m_agent.FindClosestEdge(out hit);

            if (hit.distance > 0.5)
            {
                var t = npc.transform.forward * 10.0f * Time.deltaTime;
                m_controller.Move(t);
            }
            else
            {
                var t = -npc.transform.forward * 0.2f;
                m_controller.Move(t);

                _status = Status.Rotate;
                var rotate = Quaternion.FromToRotation(npc.transform.forward, -npc.transform.forward);
                _target_rotate = npc.transform.rotation * rotate;
            }
        }

        if (_status == Status.Rotate)
        {
            var r= Quaternion.RotateTowards(npc.transform.rotation, _target_rotate, 3.0f);
            npc.transform.rotation = r;

            float dot = Quaternion.Dot(r, _target_rotate);

            if (dot<(1+10e-7)&&dot>(1 - 10e-7))
            {
                npc.transform.position += npc.transform.forward * 10.0f * Time.deltaTime;
                npc.transform.rotation = _target_rotate;
                _status = Status.Front;
            }
        }
    }

    public override void Reason(GameObject npc, Animator ani)
    {
        if (Vector3.Distance(playerTransform.position, npc.transform.position) < 20)
        {
            Ray lookRay = new Ray(npc.transform.position + new Vector3(0, 4, 0), npc.transform.forward);
            RaycastHit hit;
            if (Physics.Raycast(lookRay, out hit, 20))
            {
                if (hit.collider.gameObject.name == "Player")
                {
                    Debug.DrawRay(npc.transform.position + new Vector3(0, 4, 0), npc.transform.forward, Color.red, 20f);
                    fsm.PerformTransition(Transition.SeePlayer);
                    ani.SetFloat("speed", 1.0f);
                }
            }
        }
    }
}