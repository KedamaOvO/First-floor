﻿using Entity;
using UnityEngine;
using UnityEngine.AI;

internal class Enemy : EntityBase
{
    private Animator ani;
    private FSMSystem fsm;
    public GameObject camera;

    private NavMeshAgent m_agent;
    private CharacterController m_controller;

    // Use this for initialization
    private void Start()
    {
        m_agent = GetComponent<NavMeshAgent>();
        m_controller = GetComponent<CharacterController>();
        ani = GetComponent<Animator>();
        InitFSM();
    }

    private void InitFSM()
    {
        fsm = new FSMSystem();

        FSMState patrolState = new PatrolState(fsm, this.gameObject);
        fsm.AddState(patrolState);
        patrolState.AddTransition(Transition.SeePlayer, StateID.Chase);
        FSMState chaseState = new ChaseState(fsm, this.gameObject);
        fsm.AddState(chaseState);
        chaseState.AddTransition(Transition.LostPlayer, StateID.Patrol);
        FSMState attackState = new AttackState(fsm, this.gameObject);
        fsm.AddState(attackState); 
        attackState.AddTransition(Transition.CanAttack,StateID.See); 

    }

    private Vector3 m_moveVelocity;
    private Vector3 m_gravity;

    // Update is called once per frame
    protected override void Update()
    {
        base.Update();
        fsm.Update(this.gameObject, this.ani);

        //重力
        m_gravity = Physics.gravity * Time.deltaTime;
        m_controller.Move(m_moveVelocity * Time.deltaTime + m_gravity * Time.deltaTime);
        m_moveVelocity += m_gravity;

        if (m_controller.isGrounded)
        {
            m_moveVelocity = Vector3.zero;
            ResetStatusFlag(EntityStatus.FirstJump);
            m_agent.Warp(transform.position);//Warp非常重要
        }

        Debug.Log("Enemy"+m_controller.isGrounded);

        m_agent.nextPosition = transform.position;
    }

    public void Jump()
    {
        m_moveVelocity = transform.forward;
        m_moveVelocity.y = 1.0f;
        m_moveVelocity.Normalize();
        m_moveVelocity *= 12.0f;

        SetStatusFlag(EntityStatus.FirstJump);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            EntityBase monster = other.GetComponent<EntityBase>();
            EntityBase player = other.GetComponent<EntityBase>();
            monster.TakeDamage(3);
        }
    }
}