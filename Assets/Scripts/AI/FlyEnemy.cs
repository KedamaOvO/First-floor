﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Entity;


internal class FlyEnemy : EntityBase  {
	private Vector3  Gravity = new Vector3(0, 0f, 0);
	private Animator ani;
	private FSMSystem fsm;
	private NavMeshAgent f_agent;
	private CharacterController f_controller;

	// Use this for initialization
	void Start () {
        f_agent = GetComponent<NavMeshAgent>();
        f_controller = GetComponent<CharacterController>();
		ani = GetComponent<Animator>();
		InitFSM();
	}
	private void InitFSM()
	{
		fsm = new FSMSystem();

		FSMState patrolState = new PatrolState(fsm, this.gameObject);
		fsm.AddState(patrolState);
		patrolState.AddTransition(Transition.SeePlayer, StateID.Chase);


		FSMState chaseState = new ChaseState(fsm, this.gameObject);
		fsm.AddState(chaseState);
		chaseState.AddTransition(Transition.LostPlayer, StateID.Patrol);
		FSMState attackState = new AttackState(fsm, this.gameObject);
		//fsm.AddState(attackState);
		//attackState.AddTransition(Transition.CanAttack, StateID.See);

	}
	private Vector3 m_moveVelocity;
	
	// Update is called once per frame
	protected override void Update()
	{
		base.Update();
		fsm.Update(this.gameObject, this.ani);
        if (m_controller.isGrounded == false)
		{
			m_moveVelocity = Vector3.zero+new Vector3(0,4,0) ;			
            f_agent.Warp(transform.position);//Warp非常重要
		}
        Debug.Log("flyEnemy"+m_controller.isGrounded);
        f_agent.nextPosition = transform.position;

	}
	private void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.tag == "Player")
		{
			EntityBase flymonster = other.GetComponent<EntityBase>();
			EntityBase player = other.GetComponent<EntityBase>();
            flymonster.TakeDamage(2);

		}
	}
}
