﻿using Entity;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AttackState : FSMState {
    private Status _status = Status.Idle;
    private enum Status
    {
        Idle,
        Attack_1,
        Attack_2,
        Attack_3

    }
	private Transform playerTransform; 
	private EntityBase m_self; 
    private EntityBase m_player;
    private Transform playerTransfomrm;
    private Animator m_animator;
    private NavMeshAgent m_agent;
    private CharacterController m_controller;

    public AttackState (FSMSystem fsm,GameObject obj):base (fsm)
    {
		m_animator = obj.GetComponent<Animator>();
		m_agent = obj.GetComponent<NavMeshAgent>();
		m_controller = obj.GetComponent<CharacterController>();
		m_self = obj.GetComponent<EntityBase>();
        stateID = StateID.Attack;
        playerTransfomrm = GameObject.Find("Player").transform;
		m_player = playerTransform.GetComponent<EntityBase>();
    }
    public override void Act(GameObject npc,Animator ani)
    {

              if(_status==Status.Idle)
              {
                  NavMeshHit hit;
                  m_agent.FindClosestEdge(out hit);
                  if(hit.distance<5)
                  {
                      ani.SetBool("Idle",true);

                  }
                  _status = Status.Attack_1;
              }
              if(_status==Status.Attack_1)
              {
                  NavMeshHit hit;
                  m_agent.FindClosestEdge(out hit);
                  if(hit.distance<3)
                  {
                      ani.SetBool("Attack_1",true); 
                  }
                  _status = Status.Attack_2;
              }
        if (_status == Status.Attack_2)
        {
        	NavMeshHit hit;
        	m_agent.FindClosestEdge(out hit);
        	if (hit.distance < 1)
        	{
        		ani.SetBool("Attack_2", true);
        	}
                  _status = Status.Idle;
        }
        //NavMeshHit hit;
        //m_agent.FindClosestEdge(out hit);
        //if (hit.distance >10)
        //  {
        //      ani.SetBool("Idle", true);
        //  }
        //if (hit.distance < 5)
        //{
        //    ani.SetBool("Attack_1", true);
        //}
        //if (hit.distance < 2)
        //{
        //    ani.SetBool("Attack_2", true);
        //}
    }
    public override void Reason(GameObject npc,Animator ani)
    {
        if (Vector3.Distance(playerTransform.position, npc.transform.position) < 5)
        {
            Ray lookRay = new Ray(npc.transform.position + new Vector3(0, 4, 0), npc.transform.forward);
            RaycastHit hit;
            if (Physics.Raycast(lookRay, out hit, 20))
            {
                if (hit.collider.gameObject.name == "Player")
                {
                    Debug.DrawRay(npc.transform.position + new Vector3(0, 4, 0), npc.transform.forward, Color.red, 5f);
                    fsm.PerformTransition(Transition.SeePlayer);
                    ani.SetFloat("speed", 1.0f);
                }
            }
         
        }
    }
}
