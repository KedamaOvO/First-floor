﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimleFSM : MonoBehaviour {

    public enum State
    {
        Idle,
        Patrol,
        Chase,
        Attack,
        See
    }
    private State state = State.Idle;

	// Update is called once per frame
	void Update () {
        switch (state)
        {
            case State.Idle:
                ProcessStateIdle();
                break;
            case State.Patrol:
                ProcessStatePatrol();
                break;
            case State.Chase:
                ProcessStateChase();
                break;
            case State.Attack:
                ProcessStateAttack();
                break;
            default:
                break;
        }
    }

    void ProcessStateIdle()
    {

    }
    void ProcessStatePatrol()
    {

    }
    void ProcessStateChase()
    {

    }
    void ProcessStateAttack()
    {

    }
}
