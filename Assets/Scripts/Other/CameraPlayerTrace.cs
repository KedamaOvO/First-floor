﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraPlayerTrace : MonoBehaviour
{
    public float SMOOTH_TIME= 0.3f;

    private Transform m_playerTransform;
    private Vector3 currentVelocity;
    public Vector3 CAMERA_POSITION_OFFSET = new Vector3();


    void Start () 
    {
        m_playerTransform = GameObject.FindGameObjectWithTag("Player").transform;
    }

    // Update is called once per frame
    void LateUpdate () {
        transform.position = Vector3.SmoothDamp(transform.position, m_playerTransform.position + CAMERA_POSITION_OFFSET, ref currentVelocity, SMOOTH_TIME);
    }
  
}
